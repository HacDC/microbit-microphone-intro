input.onSound(DetectedSound.Loud, function () {
    if (state) {
        basic.showLeds(`
            . . . . .
            . . . . .
            . . . . .
            . . . . .
            . . . . .
            `)
        state = 0
    } else {
        basic.showLeds(`
            # # # # #
            # # # # #
            # # # # #
            # # # # #
            # # # # #
            `)
        state = 1
    }
})
let state = 0
state = 0
