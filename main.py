def on_sound_loud():
    global state
    if state:
        basic.show_leds("""
            . . . . .
            . . . . .
            . . . . .
            . . . . .
            . . . . .
            """)
        state = 0
    else:
        basic.show_leds("""
            # # # # #
            # # # # #
            # # # # #
            # # # # #
            # # # # #
            """)
        state = 1
input.on_sound(DetectedSound.LOUD, on_sound_loud)

state = 0
state = 0